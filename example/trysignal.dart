import 'dart:math';
import 'dart:io';
import 'package:path/path.dart' as p;

// class SmoothedZScore {
int lag = 5;
num threshold = 10;
num influence = 0.5;

num sum(List<num> a) {
  num s = 0;
  for (int i = 0; i < a.length; i++) s += a[i];
  return s;
}

num mean(List<num> a) {
  return sum(a) / a.length;
}

num stddev(List<num> arr) {
  num arrMean = mean(arr);
  num dev = 0;
  for (int i = 0; i < arr.length; i++)
    dev += (arr[i] - arrMean) * (arr[i] - arrMean);
  return sqrt(dev / arr.length);
}

List<int> smoothedZScore(List<num> y) {
  if (y.length < lag + 2) {
    throw 'y data array too short($y.length) for given lag of $lag';
  }

  // init variables
  List<int> signals = List.filled(y.length, 0);
  List<num> filteredY = List<num>.from(y);
  List<num> leadIn = y.sublist(0, lag);

  var avgFilter = List<num>.filled(y.length, 0);
  var stdFilter = List<num>.filled(y.length, 0);
  avgFilter[lag - 1] = mean(leadIn);
  stdFilter[lag - 1] = stddev(leadIn);

  for (var i = lag; i < y.length; i++) {
    if ((y[i] - avgFilter[i - 1]).abs() > (threshold * stdFilter[i - 1])) {
      signals[i] = y[i] > avgFilter[i - 1] ? 1 : -1;
      // make influence lower
      filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i - 1];
    } else {
      signals[i] = 0; // no signal
      filteredY[i] = y[i];
    }

    // adjust the filters
    List<num> yLag = filteredY.sublist(i - lag, i);
    avgFilter[i] = mean(yLag);
    stdFilter[i] = stddev(yLag);
  }

  return signals;
}

void main() {
  File file = File('testread.txt');
  var fileContent = file.readAsStringSync();
  print(fileContent);
  

  List<int> audio = [
    -42,
    48,
    141,
    234,
    285,
    237,
    87,
    -99,
    -255,
    -345,
    -366,
    -309,
    -180,
    258,
    510,
    696,
    585,
    204,
    -255
  ];
  print(audio.length);
  List<int> result = smoothedZScore(audio);
  print(result);
  print(result.length);
}
// }
