import 'dart:io';
import 'package:fftea/fftea.dart';

void main() {
  // var myFile = File('sampledata.txt');
  final stopwatch = Stopwatch()..start();

  var myFile = File('TestInputForMobile.txt');

  var read1 = myFile.readAsLinesSync();
  // print(read1);
  var rowNum = read1.length;
  var columnNum = 1024;
  // var inputList = doubleList(read1, rowNum, columnNum);

// ****** FFT PART ******
  List<List<double>> fftOutReal = [];
  List<List<double>> fftOutImag = [];

  for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
    var rowData = read1[rowIndex].split(' ');
    rowData.removeWhere((item) => ['', null, false, 0].contains(item));
    List<double> doubleVal = [];
    for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
      doubleVal.add(double.parse(rowData[columnIndex].toString()));
    }
    var fft_calculator = FFT(columnNum);
    var spectrum_data = fft_calculator.realFft(doubleVal);

    // print(spectrum_data);
    List<double> fftOutRealRow = [];
    List<double> fftOutImagRow = [];
    for (int i = 0; i < columnNum; i++) {
      fftOutRealRow.add(double.parse(spectrum_data[i].x.toString()));
      fftOutImagRow.add(double.parse(spectrum_data[i].y.toString()));

      // print(spectrum_data[i]);
    }
    fftOutReal.add(fftOutRealRow);
    fftOutImag.add(fftOutImagRow);
  }

  print('doSomething() executed in ${stopwatch.elapsedMilliseconds}');
  print('doSomething() executed in ${stopwatch.elapsed}');
  print('doSomething() executed in ${stopwatch.elapsedTicks}');
  // print(fftOutReal);
  // print(fftOutImag);

  // String fftOutRealFile = 'TestOutputFFTReal.txt';
  // String fftOutImagFile = 'TestOutputFFTImag.txt';
  // List<List<double>> expectedFftOutReal = readFileAsDoubleList(fftOutRealFile);
  // List<List<double>> expectedFftOutImag = readFileAsDoubleList(fftOutImagFile);

  // List<double> realdiff = findDiff(fftOutReal, expectedFftOutReal);
  // print(realdiff);
  // print('\n *********************** \n');
  // List<double> imagdiff = findDiff(fftOutImag, expectedFftOutImag);
  // print(imagdiff);
}

// ignore: lines_longer_than_80_chars
List<double> findDiff(
    List<List<double>> foundList, List<List<double>> expectedList) {
  List<double> error = [];
  var rowNum = foundList.length;
  var columnNum = foundList[0].length;
  for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
    // List<double> diff = [];
    double diff = 0;
    for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
      double d = (expectedList[rowIndex][columnIndex] -
              foundList[rowIndex][columnIndex])
          .abs();
      diff += d;
    }
    error.add(diff);
  }

  return error;
}

List<List<double>> readFileAsDoubleList(String filePath) {
  // read File
  var myFile = File(filePath);
  var readLines = myFile.readAsLinesSync();

  var rowNum = readLines.length;
  var columnNum = 1024;

  List<List<double>> sampleDouble = [];

  for (int rowIndex = 0; rowIndex < rowNum; rowIndex++) {
    var rowData = readLines[rowIndex].split(' ');
    rowData.removeWhere((item) => ['', null, false, 0].contains(item));
    List<double> doubleVal = [];
    for (int columnIndex = 0; columnIndex < columnNum; columnIndex++) {
      doubleVal.add(double.parse(rowData[columnIndex].toString()));
    }
    sampleDouble.add(doubleVal);
  }

  return sampleDouble;
}
